# Hearth Campaign

Tu as rejoins le clan Kyneka🌺et tu souhaites afficher l'écusson Master sur ton profil de clan, voici les regles de notre jeu "Hearth Campaign" pour permettre a tous nos membres d'y arriver.

# Le principe

Toutes les personnes du clan pourront **gagner des vies jusqu'a un maximum de 3**. Chaque vie sera le reflet de ton implication dans notre clan pour chaque GDC.

L'objectif de ce jeu est simple, ne pas perdre de vie car **si tu tombes a 0 vie**, la sentence sera irrévocable et malheuresement **tu seras exclus**.

## Gagner des vies
Pour gagner tes vies, rien de plus simple:

* **Rejoins notre serveur Discord**: tu débloqueras ta vie dorée (💛) 
* **Tu as correctement utilisé tes 14 tickets et obtenus 63 trophés lors de la précédente GDC**: Félicitations, tu gagnes automatiquement 2 vies car tu as su briller lorsque tu représentais le clan ! (❤️❤️)
* **Une nouvelle saison de GDC commence**: tu gagnes 1 vie si tu n'as pas été pénalisé lors de la saison précédente (❤️)

La vie dorée te permettra de subir une pénalité sans toucher a tes vies rouges ❤️ .

**Attention**: la vie dorée ne se recharge qu'une fois par mois, lors de la premiere saison GDC du mois. Si la vie dorée est consommée elle se transformera en vie orange 🧡. Si par malheur la vie orange est consommée elle aussi, elle se transformera en 🖤 et tu devras attendre la premiere GDC du mois prochain pour retrouver ta vie dorée 💛 comme neuve !

_Exemple:_
* _Tu es sur le serveur Discord et tu as fait un total de 63 trophés lors de la précédente GDC: **+** 💛❤️❤️_
* _Tu n'es pas sur le serveur Discord mais tu n'as pas pénalisé le clan lors de la précédente GDC: **+** ❤️_

Garde en tête que tu ne pourras jamais aller au dela de 3 vies.

## Perdre des vies
Pas de panique ! Si tu suis les consignes pendant les GDC, tu n'en perdras pas:

* **Tu n'as joué qu'entre 10 et 13 tickets**: tu perdras 1 vie ❤️ qui se transformera en vie malus 🖤
* **Tu n'as joué qu'entre 6 et 9 tickets**: tu perdras 2 vies ❤️❤️ qui se transformeront en vie malus 🖤🖤
* **Tu as joué en solo sans justification**: tu perdras 2 vies ❤️❤️ qui se transformeront en vie malus 🖤🖤
* **Tu as joué moins de 6 tickets**: tu perdras toutes tes vies et la vie dorée ne pourra pas te sauver 🖤🖤🖤

_Exemple:_
* _Tu as eu un empêchement et tu n'as pas pu utiliser tes tickets le mercredi (-4 tickets): ❤️ **>** 🖤_
* _Tu ne t'es pas organisé correctement avec les membres du clan et tu as fais la GDC en solo: ❤️❤️ **>** 🖤🖤_

Si par malheur **tu perds toutes tes vies** car tes actions (ou inaction) auront lourdement pénalisé tous les membres du clan, **tu seras expulsé du clan**. 

# FAQ

## Cas particuliers

### Mes équipiers ne jouent plus
N'attends pas, préviens rapidement les membres du Staff pour que l'on trouve une solution ensemble. Mais ne t'en fais pas, tu ne pourras pas être personnellement pénalisé par la faute des autres.

### La premiere saison GDC du mois
Nous déterminons que la premiere saison GDC du mois commence lors du premier mercredi du mois. Exemple: le 1er juin 2022 est un mercredi, alors la premiere saison GDC du mois de juin sera celle du 1er juin 2022. Si le 1er du mois tombe un dimanche, la premiere saison GDC du mois sera celle qui commencera le mercredi qui suivra le dimanche 1er.

## Questions

### Est ce que je garde mes vies d'une saison a l'autre?
Oui ! Et c'est valable pour toutes tes vies, les bonnes comme les mauvaises. Exemple:

### Je vais avoir une pénalité, comment ca marche? 
Aie ! Si tu possedes une vie dorée 💛 ou orange 🧡 alors ce sont elles qui seront consommées en premier, sinon tu n'en possedes pas ou plus, tu perdras une vie rouge ❤️

### Comment fonctionne la vie dorée 💛?
Bonne question ! La vie dorée 💛 est la meilleure vie du jeu, elle te permettra de subir 1 pénalité sans impacté tes autres vies rouges ❤️. Pour l'obtenir il te suffit simplement de faire partie de notre serveur Discord.
Une fois en ta possession, la vie dorée 💛 prendra automatiquement la premiere pénalité que tu pourrais recevoir, la transformant ainsi en vie orange 🧡.  Cette vie se régenere automatiquement lors de la premiere saison GDC du mois.

### Ou est ce que je peux voir mes vies?
Sur Discord ! Les membres du Staff mettront a jour ton pseudo avec ton compteur de vie.

### Je dois perdre 2 vies, comment ca marche?
Si tu as une vie dorée 💛 elle se transformera en vie 🧡 qui se transformera en vie 🖤. (avant: 💛❤️❤️ | apres: 🖤❤️❤️)
Si tu as 3 vies ❤️, 2 d'entres elles se transformeront en vie 🖤 (avant: 🧡❤️❤️ | apres: 🖤🖤❤️)
Si tu n'as que 2 vies ❤️ alors tu perds toutes tes vies et tu seras exclus du clan (alors fais bien attention a conserver tes vies)
